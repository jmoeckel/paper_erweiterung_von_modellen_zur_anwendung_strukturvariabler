\section*{EINLEITUNG}
Modelle aus dem Bereich der Gebäudesimulation finden ihre Verwendung oft in der Untersuchung von Kühlen, Heizen, Lüften oder von wirtschaftlichen Aspekten. Im Allgemeinen werden die erforderlichen zeitinvarianten Modelle als differentiell-algebraische Gleichungen ausgedrückt, welche mit passenden numerischen Lösern simuliert werden können.\absatz{}

In der Gebäudesimulation können Modelle von Gebäuden und Stadtvierteln entsprechend ihrer räumlichen Auflösung in drei verschiedene Typen unterschieden werden \parencite{NytMIR14}, welche hier als Detaillierungsgrad bezeichnet werden:
\begin{Description}
\item[\namedlabel{dsc:0D}{0D-Modelle}]Stark vereinfachte und niedrig aufgelöste Modelle zur Modellierung von Gebäudegruppen bis hin zu ganzen Stadtvierteln.
\item[\namedlabel{dsc:1D}{1D-Modelle}]Detailliertere Modelle einzelner Gebäude oder einiger Komponenten davon, welche meistens in thermische Zonen unterteilt werden und in denen Elemente in einer Dimension diskretisiert sind. 
\item[\namedlabel{dsc:3D}{3D-Modelle}]Sehr detaillierte dreidimensionale Modelle von Luftvolumen und den umgebenden Elementen eines Gebäudes  oder einer thermischen Zone.
\end{Description}  

Fortschritte auf dem Gebiet der Rechenleistung und die Verbreitung von Modellierungssoftware ermöglichen seit Beginn der 1990er Jahre die  Entwicklung von Simulationsmodellen, welche reale Systeme immer detaillierter und exakter abbilden. Bereits zur Jahrtausendwende wurde festgestellt, dass die signifikante Zunahme an Komplexität dem eigentlichen Prinzip von einfacher Modellierung entgegensteht und zu erheblichen Problemen bei z.B. Validierung, Lösung, Verständnis und Wartung solcher Modelle führt \parencites{Pid96,ArtSDLM99,ChwBP00}. 

Während weitere Probleme wie z.B. zu hohe Simulationszeiten durch technisches Equipment gelöst werden können, bleiben die genannten Punkte modellbezogene Probleme. Daraus folgend wird in \parencite{ChwBP00} das Fazit gezogen, dass die Suche nach Lösungsstrategien zur Behandlung von komplexen und großen Simulationsmodellen der falsche Ansatz ist. Vielmehr müsse die Fragestellung lauten, wie komplexe Modelle vermieden werden können.\absatz{}

Insbesondere in der Gebäudesimulation ist es oft nicht erforderlich, während der gesamten Simulation denselben Detaillierungsgrad, z.B. hochaufgelöste 3D-Modelle, zu verwenden. Effizienter wäre es, verschiedene Modelle mit unterschiedlichem Detaillierungsgrad innerhalb einer Simulation so zu verwenden, dass das simulierte Modell bei gleichbleibender Genauigkeit immer so einfach wie möglich ist. Dies wird durch strukturvariable Simulationen erreicht.\absatz{}

Strukturvariable Simulationen basieren auf adaptiv anpassbaren differentiell-algebraischen Gleichungssystemen \parencite{ElmCO93}. Ein grundlegender Überblick zur Anwendung von adaptiven Verfahren ist in \parencite{Bre08} gegegeben. Hier wird insbesondere die Behandlung der Adaptivität von Simulationen, die Behandlung diskreter Elemente in einer kontinuierlichen Simulation sowie die Möglichkeiten mehrerer Simulationsumgebungen betrachtet. Adaptive Verfahren bezüglich einer Veränderlichkeit von räumlicher Diskretisierung oder Schrittweitensteuerung bei einem numerischen Verfahren sind für differentiell-algebraische Gleichungen u.a. in \parencite{KunM06} untersucht worden.\absatz{}  

Zur Vereinfachung der Modellierung wird eine strukturvariable Simulation in verschiedene Modi zerlegt, welche jeweils ein zugrundeliegendes Modell besitzen. Während einer Simulation ist stets ein Modus aktiv und die Simulation kann durch eine Transition von einem Modus zum nächsten schalten.  

Während der Simulation kann somit der Detaillierungsgrad in Abhängigkeit der aktuellen Umstände gewählt werden. Die Vorteile sind kleine Modelle, welche einfach zu warten sind, und eine hohe Portabilität. Komplexes Verhalten eines Modells muss nur bei Bedarf simuliert werden. 

In der Gebäudesimulation erlauben solche strukturvariablen Simulationen einen Wechsel der Diskretisierung bei laufender Simulation. Derartige Modiwechsel bieten die Möglichkeit, die Rechenzeiten ohne negative Auswirkungen auf die notwendige Genauigkeit zu verringern.

\begin{Description}
\item[\namedlabel{exmp}{Beispiel}] Simuliert wird das 1D-Modell eines Raumes, beobachtet wird die Innenraumtemperatur in Hinblick auf Behaglichkeit. Steigt diese über $25°C$, wird das Modell des Raumes durch eine 3D-Variante ausgetauscht, da in diesem Fall genauere, örtlich aufgelöste Ergebnisse benötigt werden.  Sinkt die Innenraumtemperatur wieder unter $25°C$, wird erneut das 1D-Modell des Raumes genutzt (siehe Abbildung \ref{fig:example_svs}).
\end{Description} 

Das Beispiel dient der Veranschaulichung einer strukturvariablen Simulation und ist dementsprechend einfach gewählt, was sich unter anderem in den verwendeten Kriterien ausdrückt. Diese verhindern zum Beispiel keine häufigen Modellwechsel bei Temperaturschwankungen um $25°C$. Des Weiteren wird im Kriterium $T<25°C$ nicht die 3D-Struktur des Modells berücksichtigt. 

Neben der Änderung des Detaillierungsgrads besteht die Option, dem Modell Komponenten hinzuzufügen bzw. daraus zu entfernen, z.B Möbel, Fenster, Innenwände. Es ist zwar möglich, solch ein Verhalten durch eventbasierte Mechanismen zu erzielen, dadurch wird die Komplexität eines Modells jedoch dramatisch gesteigert, was zu nicht-simulierbaren Modellen führen kann.

\begin{Figure}
  \centering	
  \captionsetup{type=figure}
  \includegraphics[width=\linewidth]{./img/example_svs.jpg}
	\captionof{figure}{Einfaches Beispiel einer strukturvariablen Simulation}
	\label{fig:example_svs}
\end{Figure} 

Es existieren bereits spezialisierte Umgebungen und Sprachen zur Ausführung von strukturvariablen Simulationen, wie z.B. SOL \parencite{Zim10}, MOSILAB \parencite{Nytetal05}. Hier ist es bisher nicht möglich, bereits existierende Modelle in Dymola oder Simulink zu verwenden. Dies ist generell ein großer Nachteil, da hierdurch die Verwendung bereits existierender Bibliotheken verhindert wird.\absatz{}

Das Framework \dysmo{} bietet die Möglichkeit einer derartigen Simulation mit Standardsimulationsumgebungen. Wie in \parencite{Meh14} beschrieben, kann das Framework verschiedene Simulationsumgebungen wie Dymola, OpenModelica und MATLAB/Simulink steuern. Jeder Modus einer strukturvariablen Simulation kann mit einem anderen Werkzeug implementiert und in diesem simuliert werden. Das Framework ermöglicht weiterhin den Übergangsprozess von einem Modus zum nächsten. Während die Modi in einer Simulationsumgebung erzeugt werden, spezifiziert der Modellierer die Transition in dem Framework selbst. Die eigentliche Simulation wird dann von \dysmo{} geregelt und ermöglicht es dem Modellierer, sehr leicht mit strukturvariablen Simulationen zu arbeiten.\absatz{}

In \parencite{MoeMN15} wurde gezeigt, dass schon bei wenigen Modi mit einfachen Modellen die manuelle Spezifikation der Transitionen zu einem bedeutenden Aufwand für den Modellierer führt. Der Gewinn an Rechenzeit durch die strukturvariable Simulation wird zumindest relativiert. 

In dieser Veröffentlichung wird daher ein erstes Konzept zur Modellierung vorgestellt, welches eine Teilautomatisierung der Spezifikation von Transitionen erlaubt. Es wird zunächst die Funktionsweise strukturvariabler Simulationen konzeptionell betrachtet und die Komponenten Modus und Transition vollständig eingeführt. Danach wird der erste Entwurf eines erweiterten Modellierungsansatzes vorgestellt, um strukturvariable Simulationen zu unterstützen. Es werden Implementierungsbeispiele zur Umsetzung gegeben sowie eine Anbindung an \dysmo{} vorgestellt. Abschließend werden die maßgeblichen Aspekte des Papers zusammengefasst und ein Ausblick gegeben.  
