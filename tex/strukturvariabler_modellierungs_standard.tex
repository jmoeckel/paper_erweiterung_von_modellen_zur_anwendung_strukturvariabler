\section*{ERWEITERTE MODELLIERUNG}

In diesem Kapitel wird die Beobachtung des vorangegangenen Abschnittes bezüglich der Anwendbarkeit von strukturvariablen Simulationen aufgegriffen und eine Lösung in Form eines Standards zur Modell\-erweiterung für die Anwendung in einer strukturvariablen Simulation (\SMASS{}) vorgestellt. 

\subsection*{Zielsetzung von \SMASS{}}
Das Ziel von \SMASS{} ist es, die Spezifikation einer Transition auf Komponentenebene zu generalisieren und somit die Spezifikation für ein ganzes Modell automatisiert erstellen zu können. Das Konzept von \SMASS{} ist dabei insbesondere für die Anwendung innerhalb von Bibliotheken gedacht und baut auf die in der Regel vorhandene Objekt-Klassen-Struktur auf. Bibliotheken aus dem Bereich der Gebäudesimulation enthalten zum Beispiel in der Regel das Objekt "Wand", welchem verschiedene Klassen zugeordnet sind, die unterschiedliche Modellierungen einer Wand enthalten. 

Im Rahmen von \SMASS{} wird nun für jede Klasse eine Transition zu jeder anderen Klasse in diesem Objekt spezifiziert. Der Umfang der zu spezifizierenden Transitionen pro Objekt ist damit zwar erneut $n!$, allerdings ist die Anzahl der Variationen eines Objekts in der Regel gering und somit $n$ klein. Selbiges gilt für den Umfang einer Klasse gegenüber einem vollständigen Gebäudemodell. Der Gesamtumfang der Spezifikationen ist also erheblich geringer als im Vergleich zu den angesprochenen Spezifikationen für eine vollständige strukturvariable Simulation.\absatz{}

Es ist zu beachten, dass somit im Rahmen von \SMASS{} nur Wechsel innerhalb eines Bibliotheksobjekts vorgesehen sind. Dies bedeuted einerseits eine Einschränkung in der Modellierung, andererseits erlaubt erst diese Einschränkung einen Ansatz zur Automatisierung bei der Spezifikation von Transitionen. 

Weiterhin ist zu beachten, dass für jede Klasse vorgesehen werden muss, dass sie keine Entsprechung in dem \lastMode{} besitzt (vgl. Klasse 4). Dieser Fall muss ebenfalls durch eine Spezifikation abgedeckt werden. Dies bedeuted, dass die Anzahl der zu spezifizierenden Transitionen bei $n$ Klassen nicht bei $n!$, sondern bei $n!+n$ liegt. 

\subsection*{Realisierung von \SMASS{}} 
Es gibt zwei Möglichkeiten, wie \SMASS{} realisiert werden kann. Zum einen kann eine modell-interne Lösung gewählt werden. Hier können - unabhängig von der Modellierungssprache - Spezifikationen als nicht ausgewertete Kommentare eingefügt werden. Speziell im Fall von Modelica bietet sich zudem die Verwendung von Annotationen an. Beide Lösungen bergen allerdings Nachteile in sich:
\begin{itemize}[noitemsep,topsep=0.22cm]
\item Modelle würden zwangsläufig die Informationen für die Anwendung im Rahmen einer strukturvariablen Simulation enthalten, unabhängig davon, ob der Modellierer strukturvariable Simulationen durchführen möchte oder nicht. 
\item Bei der Verwendung von Annotationen kann es zu Problemen mit Simulationsumgebungen wie Dymola oder OpenModelica kommen, da nicht Modelica-konforme Annotations-Tags verwendet werden würden.
\item Der Umfang des Quellcodes eines Modells würde enorm ansteigen. 
\end{itemize}

Gerade der letzte Punkt geht einher mit einer Zunahme an Komplexität bzw. Abnahme an Wartbarkeit eines Modells. Dies aber würde zwei Vorteile von Modellen einer strukturvariablen Simulation aufheben. Eine modell-interne Lösung  bietet sich somit nicht an. 

Zum anderen können die \SMASS{}-relevanten Daten modell-extern gespeichert werden. Hier bietet es sich an, eine zur Bibliothek parallele Struktur anzulegen. Dies hat zum einen den Vorteil eines für Modellierer nachvollziehbaren Aufbaus, zum anderen unterstützt diese Struktur Automatisierungsprozesse. Als konkretes Datenformat für die in \SMASS{} verarbeiteten Informationen wird das xml-Format genutzt.\absatz{}

Dies hat die Vorteile der einfachen Lesbarkeit für den Modellierer, des einfachen Zugangs durch externe Skripte, da jede gängige Programmiersprache über eine Vielzahl an xml-Parsern verfügt, und der Unabhängigkeit von einer konkreten Software, wie es zum Beispiel bei Datenbanken der Fall ist.

\subsection*{XML-Struktur}
Aus der Anfordung einer zur Bibliothek parallelen Struktur ergibt sich die in Abbildung \ref{fig:structure_xml} dargestellte Struktur eines xmls, welche die \SMASS{}-relevanten Daten einer Modellklasse enthält. 

\begin{Figure}
  \centering	
  \captionsetup{type=figure}
  \includegraphics[width=\linewidth]{./img/xml_general.jpg}
	\captionof{figure}{xml-Struktur von SMASS in Form eines uml-Schemas}
	\label{fig:structure_xml}
\end{Figure}

Hierbei sind:
\begin{Description}
\item[Class] Entspricht einer Klasse des dem \nextMode{} zugrundeliegenden Modells mit ID "`name". 
\item[LastClass] Entspricht einer Klasse ungleich Class mit ID "`name"' des dem \lastMode{} zugrundeliegenden Modells desselben Bibliotheksobjekts wie Class.
\item[Mapping] Beschreibung der Identifikation und des Transfers zwischen Class und LastClass.
\item[VarOld] Liste der Variablen der LastClass, die Teil des Mappings sind.  
\item[VarNew] Liste der Variablen der Class, die Teil des Mappings sind.
\item[Scaling] Numerische Operationen in der Syntax der Programmiersprache "`language"', welche im Rahmen des Mappings angewendet werden sollen.
\end{Description}

Zu beachten ist, dass VarOld und VarNew nicht auf die Nennung einer Variable beschränkt sind, sondern eine kommagetrennte Aufzählung enthalten können.\absatz{}

Um den Fall abzudecken, dass eine Modellklasse des \nextMode{} keine Entsprechung in dem zugrundeliegenden Modell des \lastMode{} hat, wird ein LastClass-Element Class hinzugefügt mit \textit{name="`empty"'}. In diesem Fall wird in LastClass für jede essentielle Variable von Class ein Mapping-Element verwendet, wobei
\begin{itemize}[noitemsep,topsep=0.22cm]
\item VarOld immer leer bleibt,
\item VarNew den Namen und
\item Scaling den Initialwert der essentiellen Variable enthält.
\end{itemize}

Die numerischen Operationen werden in einer beliebigen Programmiersprache angegeben. Die Sprache wird über das Attribut "`language"' gesetzt. Eine Einschränkung ist hier nicht nötig, da die Operationen letztendlich nur ausgelesen und dann vom übergeordneten Framework interpretiert werden. 

Hier geschieht dies mit Python. Dies ergibt sich derzeit aus der Verwendung des in Python geschriebenen Frameworks \dysmo{}. Bleibt Scaling leer, wird als Standardoperation eine Eins-zu-eins-Übernahme des Wertes der Variable in VarOld für VarNew angewandt.\absatz{}

Angewendet auf das eingangs gegebene Beispiel ergeben sich damit die in Listing \ref{lst:T_1D} und Listing \ref{lst:T_3D} aufgeführten xml-Strukturen. Um die xml-Struktur hierbei übersichtlich zu halten, wird davon ausgegangen, dass sowohl das Modell Raum1D und das Modell Raum3D jeweils eine Klasse und parallel in einer Bibliothek dem Objekt "`Raum"' zugeordnet sind. Weiterhin wird vorausgesetzt, dass die Diskretisierung des 3D-Modells sich auf eine 2x2-Zerlegung beschränkt.

Als einzige essentielle Variable des 1D-Modells wird die Innenraumtemperatur $T$ angenommen, im 3D-Modell die Temperaturen der einzelnen diskretisierten Zellen $T_{11}, T_{12}, T_{21}$ und $T_{22}$. Bei dem Scaling wird Python-Syntax verwendet, insbesondere wird von der Verwendung des numpy-Moduls ausgegangen.\absatz{}

Unabhängig von dem gegebenen Beispiel wird in beiden xml-Strukturen auf den Fall eingegangen, dass die Klasse room1D bzw. room3D keine Entsprechung in dem zugrundeliegenden Modell des \lastMode{} hat. In diesem Fall werden die Temperaturen mit $20°C$ initialisiert.\absatz{}

\lstinputlisting[language=XML, tabsize=2, frame=single, otherkeywords = {Class, Last, Mapping, VarOld, VarNew, Scaling, Variable, varname, value},  captionpos=b, caption={SMASS xml-Struktur des 1D-Raum-Modells},label={lst:T_1D}]{./xml/T_1D.xml}

\lstinputlisting[language=XML, tabsize=2, frame=single, otherkeywords = {Class, Last, Mapping, VarOld, VarNew, Scaling, Variable, varname, value},  captionpos=b, caption={SMASS xml-Struktur des 3D-Raum-Modells},label={lst:T_3D}]{./xml/T_3D.xml}

\subsection*{Verwendung im Rahmen einer strukturvariablen Simulation}
Entsprechend des vorgestellten Schemas werden für alle Klassen eines Bibliotheksobjekts xml-Dateien angelegt. Bezüglich der Spezifikation von Transitionen zwischen verschiedenen Modi muss der Modellierer zwar weiterhin  eine Klassenzuordnung festlegen, z.B. welche Wand aus dem Modell des \lastMode{} entspricht welcher Wand aus dem Modell des \nextMode{}, aber es muss keine Variablenzuordnung mehr geschehen.\absatz{}

Abbildung \ref{fig:ablauf_SMASS} zeigt den schematischen Ablauf bei einem Übergang zwischen zwei Modi im Rahmen einer strukturvariablen Simulation in Form eines Statecharts. Es wird an dieser Stelle davon ausgegangen, dass die Steuerung bzw. das Framework bereits über die Information verfügt, dass entsprechend \SMASS{} modelliert wurde und dass diese Informationen auch verwendet werden sollen.

\begin{Figure}
  \centering	
  \captionsetup{type=figure}
  \includegraphics[width=\linewidth]{./img/ablauf_svs.jpg}
	\captionof{figure}{Prozessablauf einer Transition unter Verwendung von \SMASS{}}
	\label{fig:ablauf_SMASS}
\end{Figure}

Speziell im Fall des Frameworks \dysmo{} bietet die offene in Python realisierte Implementierung sehr leicht die Möglichkeit, entsprechende Module in die Steuerung einer strukturvariablen Simulation einzubinden, was ebenso für die Abfrage gilt, ob unter Verwendung von \SMASS{} simuliert werden soll oder nicht.

Es sollte beachtet werden, dass die hier beschriebenen Prozesse auch automatisiert als Präprozess ausgeführt werden können, was die Rechenzeiten während einer Simulation weniger belasten dürfte. 

\subsection*{Fazit}
Die präsentierte Verwendung von \SMASS{} bietet dem Modellierer unter dem Aspekt strukturvariabler Simulationen erhebliche Vorteile. Hierzu zählen u.a.:

\begin{itemize}[noitemsep,topsep=0.22cm]
\item Keine Veränderungen bereits existierender Modelle durch Nutzung einer modell-externen Informationsspeicherung.
\item Modelle können sowohl unter Verwendung von \SMASS{} als auch konventionell verwendet werden.
\item Geringerer Arbeitsaufwand für einen Modellierer, da \SMASS{} darauf aufbaut, dass die für eine strukturvariable Simulation relevanten Informationen von den Entwicklern von Bibliothekskomponenten zur Verfügung gestellt werden. 
\item Die standardisierte xml-Struktur bietet erhebliche Möglichkeiten bei der Automatisierung von Präprozessen. Durch eine Erweiterung der genannten Elemente/Attribute ist es zudem möglich, weitere Automatisierungen zu realisieren.
\end{itemize}

Grundsätzlich wird mit \SMASS{} der als kritisch beobachtete Arbeitsaufwand des Präprozesses im Rahmen einer strukturvariablen Simulation erheblich reduziert. Die damit einhergehende Einschränkung bezüglich der Wahl von Modellklassen ist zwar zu berücksichtigen. Da allerdings unter dem Gesichtspunkt konsistenter und stabiler Implementierungen die ausschließliche Verwendung von Modellklassen einer Bibliothek zu empfehlen ist, kann diese Einschränkung vernachlässigt werden. 