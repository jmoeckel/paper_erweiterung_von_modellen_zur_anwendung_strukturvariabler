\section*{STRUKTURVARIABLE SIMULATIONEN}
Strukturvariable Simulationen basieren auf einer variableabhängigen Zerlegung einer Simulation in mehrere Abschnitte. Sie sind insbesondere dann von Vorteil, wenn sich unter bestimmten simulierten Bedingungen der Beobachtungschwerpunkt ändert.\absatz{} 

Schematisch bestehen strukturvariable Simulationen aus zwei Komponenten: Modi und Transitionen.

Ein {\bfseries Modus} entspricht den bereits beschriebenen Abschnitten und enthält seinerseits drei Komponenten:

\begin{Description}
\item[Modell] Ein kompilierfähiges Modell, welches die Beschreibung u.a. von zu simulierenden Objekten, physikalischem Verhalten, numerischer Diskretisierung enthält.
\item[Simulationssettings] Die gewählten Einstellungen bei der Ausführung der Simulation wie numerische Löser, Toleranzen, Schrittweiten. 
\item[Abbruchbedingung] Kondition, die zum Abbruch der Simulation eines Modells führt und Angabe des als nächstes zu initialisierenden Modus.
\end{Description}

Die in der Komponente Abbruchbedingung beschriebene Kondition kann in Abhängigkeit von jeder simulierten Variable bzw. Kombination aus mehreren Variablen formuliert werden. Generell sind hier alle Bedingungen denkbar, die durch logische Operationen ausdrückbar sind. Grundsätzlich verfügt ein Modus immer über genau ein Modell und genau ein Simulationssetting, kann aber mehrere Abbruchbedingungen besitzen.

Wird die Kondition einer Abbruchbedingung wahr, wird der dazugehörige Modus beendet und der als nächstes ausgezeichnete Modus initialisiert. Im Folgenden bezeichnt $K(\texttt{Modus } j)$ die Abbruchbedingung, die zu einer Initialisierung des Modus $j$ führt.\absatz{} 

Zwecks einfacherer Formulierung werden folgende Begriffe eingeführt: {\bfseries \lastMode{}} bezeichnet den terminierten Modus, {\bfseries \nextMode{}} den als nächstes zu initialisierenden Modus. Die zur Initialisierung des eines Modus zugrundeliegenden Modells notwendigen Variablen werden als {\bfseries essentiell} bezeichnet.\absatz{} 

Der Übergang zwischen dem \lastMode{} und dem \nextMode{} wird als {\bfseries Transition} bezeichnet. Eine Transition soll grundsätzlich gewährleisten, dass die simulationsübergreifenden Ergebnisse keine oder aber, wenn nicht vermeidbar, nur geringe Sprünge an Stellen enthalten, an denen von einem Modus zum nächsten gewechselt wurde. Ebenso wie ein Modus enthält eine Transition drei Komponenten:

\begin{Description}
\item[Identifikation] Bestimmung der Variablen des \lastMode{}, welche den \nextMode{} so initialisieren, dass simulationsübergreifende Ergebnisse keine oder nur geringe Sprünge aufweisen. 
\item[Transfer] Übertrag von Variablenwerten und eventuelle Skalierungen in Form von numerischen Operationen. 
\item[Speichern] Variablenwerte für simulationsübergreifende Ergebnisse speichern. 
\end{Description}

Im Rahmen der Identifikation werden zwei Punkte berücksichtigt: Zum einen der Vergleich von Namensräumen zwischen den zugrundeliegenden Modellen des \lastMode{} und des \nextMode{}. Normalerweise kann nicht davon ausgegangen werden, dass essentielle Variablen des \nextMode{} und entsprechende Variablen des \lastMode{} denselben Namen haben. 

Zum anderen kann im Allgemeinen nicht von einer Eins-zu-eins-Beziehung zwischen Variablen des \lastMode{} und des \nextMode{} ausgegangen werden. Grundsätzlich können hier vier verschiedene Klassen unterschieden werden:

\begin{Description}
\item[Klasse 1] Eine essentielle Variable in dem \nextMode{} hat eine direkte Entsprechung in dem \lastMode{}.
\item[Klasse 2] Eine essentielle Variable des \nextMode{} entspricht mehreren Variablen des \lastMode{}.
\item[Klasse 3] Mehrere essentielle Variablen des \nextMode{} entsprechen einer Variable des \lastMode{}.
\item[Klasse 4] Eine essentielle Variable des \nextMode{} hat keine Entsprechung in dem \lastMode{}.
\end{Description}

Dies hat Auswirkungen auf die Komponente Transfer. Klasse 1 ist der triviale Fall. Klassen 2 und 3 treten insbesondere auf, wenn Modi miteinander geschaltet werden, deren zugrundeliegende Modelle unterschiedlich fein diskretisiert sind, d.h. die beiden Modelle über eine unterschiedliche Anzahl an Stützstellen verfügen. Numerisch lassen sich beide Fälle durch gewichtete Mittelwertsbildung bzw. Distributionen erreichen, wobei die Wahl der Gewichte von Fall zu Fall in Abhängigkeit der betrachteten Variable entschieden werden muss.  

Klasse 4 beschreibt u.a. die Fälle, in denen im Modell des \nextMode{} Objekte modelliert sind, die keine Entsprechung im Modell des \lastMode{} haben, z.B. wenn im Modell des \nextMode{} der Einfluss von Möbeln mitbetrachtet wird, welche zuvor nicht modelliert waren. In solchen Fällen muss jeweils eine Einzelfallentscheidung getroffen werden, wie die entsprechenden essentiellen Variablen initialisiert werden.\absatz{} 

Abbildung \ref{fig:scheme_classes} stellt schematisch diese vier Klassen dar. Gegenübergestellt werden hier Variablen der dem \lastMode{} und dem \nextMode{} zugrundeliegenden Modelle. Die letzte Spalte zeigt einfache Beispiele für mögliche Zuordnungen, exemplarisch angewandt auf die Größen Temperatur $T$ und Masse $m$. Hierbei bezeichnen die hochgestellten Indizes $t$ und $i$ jeweils Variablen des \lastMode{} bzw. des \nextMode{}.\absatz{} 

Im Beispiel zu Klasse 2 wird davon ausgegangen, dass $n$ Variablen des \lastMode{} einer einzigen Variable des \nextMode{} zugeordnet werden. Um entsprechende physikalische Erhaltungssätze nicht zu verletzen wird zwischen Temperatur und Masse unterschieden. Hier wird der ungewichtete Mittelwert der Temperaturen, bzw. die Summe der Massen der $n$ Variablen als Zuordnung verwendet.

Das Beispiel zu Klasse 3 geht vom umgekehrten Fall aus: eine Variable des \lastMode{} wird $n$ Variablen des \nextMode{} zugeordnet, wobei die Zuordnungsvorschiften wieder so gewählt wurden, dass sie physikalischen Erhaltungssätzen genügen. 

\begin{Figure}
  \centering	
  \captionsetup{type=figure}
  \includegraphics[width=\linewidth]{./img/klassen.jpg}
	\captionof{figure}{Vereinfachte Darstellung der vier Identifikationsklassen mit möglichen Transfervorschriften im Fall von Temperatur und Masse}
	\label{fig:scheme_classes}
\end{Figure} 

Es wird an dieser Stelle bewusst darauf verzichtet, die behandelten Variablen genauer zu spezifizieren, um den Prozess der Identifikation nicht auf eine Modellierungssprache oder eine Simulationsumgebung einzuschränken. 

In vielen Fällen sind diese Variablen Bestandteil von  unterschiedlich modellierten Objekten wie Luftvolumina, Wandelemente oder aber ergeben sich aus der Diskretisierung eines einzigen modellierten Objekts, im Beisiel mit $n-1$ Stützstellen. Explizit wird daher in den Identifikationsklassen nur der Begriff Variable verwendet. 

Darüberhinaus ist die Indentifikation grundsätzlich davon abhängig, welche Simulationsumgebung und Modellierungssprache für die den zwei behandelten Modi zugrundeliegenden Modellen verwendet werden. Grund hierfür ist zum Beispiel, dass unterschiedliche Simulationsumgebungen unterschiedliche Methoden zur Initialisierung eines Modells verwenden, was Einfluss auf Startwerte haben kann.\absatz{} 

Die Schwierigkeiten der Identifikation sind übertragbar auf die Komponente Speichern. Auch hier müssen Unterschiede in den Namensräumen der Modelle einer strukturvariablen Simulation betrachtet werden, ebenso die Tatsache, dass eine beobachtete Größe nicht in allen Modellen existiert. Im eingangs gegebenen Beispiel soll die Innenraumtemperatur betrachtet werden. Im 1D-Modell liegen hierzu jedoch andere Informationen vor als im 3D-Modell. Ähnlich der Beschreibung über die Klassen 1 bis 4 müssen hier Übersetzungen konstruiert werden.\absatz{}

Aufgrund der ähnlichen Problematik wird im Folgenden lediglich auf die Schwierigkeiten bei der Spezifikation von Identifikation und Transfer eingegangen.\absatz{}

Die Konstruktion einer strukturvariablen Simulation ist schematisch in Abbildung \ref{fig:scheme_svs} dargestellt. Abbildung \ref{fig:statechart_svs} zeigt in Form eines Statecharts den Ablauf einer strukturvariablen Simulation - hier für drei verschiedene Modi. 

Aus beiden Abbildungen wird deutlich, dass die Anzahl an Modi und an Transitionen nicht identisch ist. Sofern alle betrachteten Modi miteinander geschaltet werden können, müssen bei $n$ Modi $n!$ Transitionen berücksichtigt werden. Dies führt auch bei wenigen berücksichtigten Modell- oder Simulationssettingvarianten im Rahmen einer strukturvariablen Simulation zu einer hohen Anzahl an zu implementierenden Transitionen, selbst wenn sich einige Übergänge zwischen Modi aufgrund von Vorüberlegungen auschließen lassen.

\begin{Figure}
  \centering	
  \captionsetup{type=figure}
  \includegraphics[width=\linewidth]{./img/content_svs.jpg} 
	\captionof{figure}{Schema der Komponenten einer strukturvariablen Simulation}
	\label{fig:scheme_svs}
\end{Figure} 

\begin{Figure}
  \centering	
  \captionsetup{type=figure}
  \includegraphics[width=\linewidth]{./img/schema_svs.jpg}
	\captionof{figure}{Zustandsdiagramm einer strukturvariablen Simulation mit drei Modi}
	\label{fig:statechart_svs}
\end{Figure}

Diese zwei Punkte der Identifikation - sowohl die Bestimmung essentieller Variablen als auch der Vergleich der Namensräume - müssen seitens des Modellierers manuell umgesetzt werden und sind derzeit nicht automatisierbar. Eventuelle Zeitgewinne durch Reduktion von Rechenzeiten werden somit durch diesen aufwendigen Präprozess erheblich eingeschränkt, können teilweise sogar annuliert werden. Trotz seiner Vorteile gegenüber klassicher Modellierung weist das Modellierungskonzept der strukturvariablen Simulation damit einen erheblichen Mangel an Anwendbarkeit auf.