# Erweiterung von Modellen zur Anwendung strukturvariabler Simulationen in Modelica

Dieses Repository enthält die (kompilierbaren) Source-Dateien zu oben genannter Arbeit. 

## Kurzfassung

Bei der Modellierung und Simulation von Gebäuden sind oft Kompromisse zwischen Genauigkeit und Rechenzeit notwendig. Aus Gründen der Performanz sind 1D-Modelle zu bevorzugen, während aus Sicht der Genauigkeit von Ergebnissen 3D-Modelle verwendet werden sollten. Strukturvariable Simulationen stellen einen Ansatz dar, nur dann komplexe und rechenintensive Modelle zu simulieren, wenn dies notwendig ist. Die Spezifikation der Übergänge zwischen zwei Modellen muss derzeit manuell implementiert werden und bedeutet schon bei einfachen Modellen einen sehr hohen Arbeitsaufwand. In dieser Veröffentlichung wird der Entwurf einer standardisierten Erweiterung von Modelica-Modellen für die vereinfachte Einbindung im Rahmen einer strukturvariablen Simulation vorgestellt. Es wird gezeigt, dass dies die teilautomatisierte Erstellung der Spezifikation eines Übergangs zwischen zwei Modellen ermöglicht. 

## Konferenz
Diese Arbeit wurde als Beitrag zur CESBP Central European Symposium on Building Physics/BauSIM 2016 Konferenz in Dresden, Deutschland (14.-16. September 2016) akzeptiert und dort vorgestellt. Sie ist teil des Conference Proceedings ([hier](http://s.fhg.de/CESBP_BauSim)) bzw. ist auf ResearchGate verfügbar ([hier](http://tinyurl.com/hbluxdf)).